import loginStore from './modules/login'
import userStore from './modules/user'
import consultStore from './modules/consult'

export default () => {
  return {
    login: loginStore(),
    user: userStore(),
    consult: consultStore()
  }
}

export const useStore = () => {
  return {
    login: loginStore(),
    user: userStore(),
    consult: consultStore()
  }
}
