import { defineStore } from 'pinia'
import { Consult } from '@/types/consult'

export default defineStore('consult', {
  state: () => {
    return {
      info: {} as Consult // 问诊信息
    }
  },
  actions: {
    // 设置极速问诊类型（0：普通、1：三甲）
    setIllnessType (payload: string) {
      this.info.illnessType = payload
    },
    // 设置科室
    setDepId (payload: string) {
      this.info.depId = payload
    },
    // 设置问诊图文信息
    setIllness (illness: Partial<Consult>) {
      this.info.illnessDesc = illness.illnessDesc!
      this.info.illnessTime = illness.illnessTime!
      this.info.consultFlag = illness.consultFlag!
      this.info.pictures = illness.pictures
    },
    // 设置患者
    setPatientId (patientId: string) {
      this.info.patientId = patientId
    },
    // 清空
    clear () {
      this.info = {} as Consult
    }
  },
  persist: true
})
