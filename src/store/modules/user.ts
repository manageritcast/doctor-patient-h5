import { defineStore } from 'pinia'
import { ApiRes } from '@/types/data'
import { User } from '@/types/user'
import request from '@/utils/request'

export default defineStore({
  id: 'user',
  state: () => {
    return {
      // 用户信息
      userInfo: {} as User
    }
  },
  actions: {
    async getUserInfo () {
      const res = await request.get<ApiRes<User>>('patient/myUser')

      this.userInfo = res.data.data
    },
    logout () {
      this.userInfo = {} as User
    }
  }
})
