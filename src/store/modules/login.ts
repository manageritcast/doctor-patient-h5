import { defineStore } from 'pinia'
import { LoginRes } from '@/types/login'

export default defineStore('login', {
  state: () => {
    return {
      info: {} as LoginRes,
      redirectUrl: ''
    }
  },
  actions: {
    setLoginInfo (payload: LoginRes) {
      this.info = payload
    },
    logout () {
      this.info = {} as LoginRes
    },
    setRedirectUrl (redirectUrl: string) {
      this.redirectUrl = redirectUrl
    }
  },
  persist: true
})
