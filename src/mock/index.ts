import Mock from 'mockjs'
import { MockMethod } from 'vite-plugin-mock'

// 拦截查询所有年级信息的请求
Mock.mock('/api/user/list', 'get', () => {
  return Mock.mock({
    code: 200,
    message: 'ok',
    data: {
      'items|10': [
        {
          avatar: /http:\/\/www(\.[a-zA-Z0-9]{0,10}\.com\/file\/[a-zA-Z0-9]{0,10})\.png/,
          create_time: '@date @time',
          email: '@email',
          id: '@integer(1,1000)',
          phone: /^[1][3-9]\d{9}$/,
          remark: '@ctitle',
          role: '管理员',
          role_id: 2,
          status: /0|1/,
          update_time: '@date @time',
          username: '@name'
        }
      ],
      pagination: {
        total: 30,
        page: 1
      }
    }
  })
})

const rules: MockMethod[] = [
  {
    url: '/patient/message/list',
    method: 'get',
    timeout: 1000,
    response: () => {
      const data = []
      for (let i = 0; i < 10; i++) {
        data.push(
          Mock.mock({
            id: '@id',
            avatar: '@image("100x100")',
            title: '@ctitle(3,10)',
            lastContent: '@ctitle(10,40)',
            sendTime: '@datetime()'
          })
        )
      }
      return {
        code: 10000,
        message: '获取数据成功',
        data
      }
    }
  }
]

export default rules
