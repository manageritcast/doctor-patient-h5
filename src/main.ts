import { createApp } from 'vue'
import App from './App.vue'
// 先引入第三方的，再引入我们自己的，这样样式才能被覆盖，起作用
import 'normalize.css'
import 'vant/lib/index.css'
import '@/assets/styles/common.scss'

import router from '@/router'
import '@/mock'
import 'virtual:svg-icons-register'
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'
import { createPinia } from 'pinia'
const pinia = createPinia()
pinia.use(piniaPluginPersistedstate)

const app = createApp(App)
app.use(router)
app.use(pinia)

app.mount('#app')
