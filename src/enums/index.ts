// 问诊类型
export enum ConsultType {
  ConsultDoctor = '1',
  ConsultFast = '2',
  ConsultMedicines = '3'
}

// 问诊时间，以1自增可以省略
export enum IllnessTime {
  Week = 1,
  Month,
  HalfYear,
  More
}

// 订单类型
// 1待支付2待接诊3咨询中4已完成5已取消   问诊订单
// 10待支付11待发货12待收货13已完成14已取消   药品订单
export enum OrderType {
  ConsultPay = 1,
  ConsultWait = 2,
  ConsultChat = 3,
  ConsultComplete = 4,
  ConsultCancel = 5,
  MedicinePay = 10,
  MedicineSend = 11,
  MedicineTake = 12,
  MedicineComplete = 13,
  MedicineCancel = 14
}

// 消息类型 1文字21卡片-患者病情 22卡片-处方信息31通知-普通通知（白底黑色）32通知-温馨提示33通知-订单取消（灰色底黑字）4图片
export enum MsgType {
  MsgText = 1,
  MsgImage = 4,
  CardPat = 21,
  CardPre = 22,
  CardEvaForm = 23,
  CardEva = 24,
  Notify = 31,
  NotifyTip = 32,
  NotifyCancel = 33
}

// 处方状态
export enum PrescriptionStatus {
  NotPayment = 1,
  Payment = 2,
  Invalid = 3
}

// 快递状态
export enum ExpressStatus {
  Delivered = 1,
  Received = 2,
  Transit = 3,
  Delivery = 4,
  Signed = 5
}
