import { CodeType, User } from '@/types/user'
import { request } from '@/utils/request2'

// 第三方登录-QQ登录
export const loginByQQ = (openId: string) => {
  return request<User>('login/thirdparty', 'POST', { openId, source: 'qq' })
}

// 发送验证码
export const sendMobileCode = (mobile: string, type: CodeType) =>
  request('/code', 'GET', { mobile, type })

// 三方登录-绑定手机号
export const loginBind = (data: {
  mobile: string
  code: string
  openId: string
}) => request<User>('login/binding', 'POST', data)
