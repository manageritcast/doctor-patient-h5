import { OrderPre, AddressItem, OrderDetail } from '@/types/order'
import { request } from '@/utils/request2'
import { Logistics } from '@/types/order'

// 药品订单-支付药款页面-根据处方信息计算药款
export const getOrderPre = (prescriptionId: string) => {
  return request<OrderPre>(
    `patient/medicine/order/pre?prescriptionId=${prescriptionId}`
  )
}

// 药品订单-获取收获地址
export const getAddress = () => {
  return request<AddressItem[]>('patient/order/address')
}

// 药品订单-创建药品订单
export const createMedicalOrder = (data: {
  id: string
  addressId: string
  couponId?: string
}) => {
  return request<{ id: string }>('patient/medicine/order', 'POST', data)
}

// 药品订单-获取药品订单详情
export const getMedicalOrderDetail = (id: string) => {
  return request<OrderDetail>(`patient/medicine/order/detail/${id}`)
}

// 药品订单-获取物流轨迹
export const getOrderLogistics = (id: string) => {
  return request<Logistics>(`patient/order/${id}/logistics`)
}
