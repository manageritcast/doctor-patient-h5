import { createRouter, createWebHistory } from 'vue-router'
import useStore from '@/store'

const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: '/',
      component: () => import('@/views/layout/index.vue'),
      redirect: '/home',
      children: [
        {
          path: 'home',
          component: () => import('@/views/home/index.vue')
        },
        {
          path: 'health',
          component: () => import('@/views/health/index.vue')
        },
        {
          path: 'message',
          component: () => import('@/views/message/index.vue')
        },
        {
          path: 'my',
          component: () => import('@/views/my/index.vue')
        }
      ]
    },
    {
      path: '/login',
      component: () => import('@/views/login/index.vue')
    },
    {
      path: '/login/callback',
      component: () => import('@/views/login/login-callback.vue')
    },
    {
      path: '/my/patient',
      component: () => import('@/views/my/patient.vue')
    },
    {
      path: '/my/consult',
      component: () => import('@/views/my/consult.vue')
    },
    {
      path: '/my/consult/:id',
      component: () => import('@/views/my/consult-detail.vue')
    },
    {
      path: '/consult/fast',
      component: () => import('@/views/consult/consult-fast.vue')
    },
    {
      path: '/consult/dep',
      component: () => import('@/views/consult/consult-dep.vue')
    },
    {
      path: '/consult/illness',
      component: () => import('@/views/consult/consult-illness.vue')
    },
    {
      path: '/consult/pay',
      component: () => import('@/views/consult/consult-pay.vue')
    },
    {
      path: '/room',
      component: () => import('@/views/room/index.vue')
    },
    {
      path: '/order/:id',
      component: () => import('@/views/order/order-detail.vue')
    },
    {
      path: '/order/pay',
      component: () => import('@/views/order/order-pay.vue')
    },
    {
      path: '/order/pay/result',
      component: () => import('@/views/order/order-pay-result.vue')
    },
    {
      path: '/order/logistics/:id',
      component: () => import('@/views/order/order-logistics.vue')
    },
    {
      path: '/test',
      component: () => import('@/views/test/index.vue')
    }
  ]
})

// 导航守卫
router.beforeEach((to, from, next) => {
  if (to.fullPath.includes('login')) {
    next()
  } else {
    const { login } = useStore()
    if (login.info.token) {
      next()
    } else {
      next(`/login?redirectUrl=${encodeURI(to.fullPath)}`)
    }
  }
})

export default router
