import { App } from 'vue'
import Icon from './Icon/index.vue'
import NavBar from './NavBar/index.vue'
import RadioButton from './RadioButton/index.vue'
import PaySheet from './PaySheet/index.vue'

export default {
  install (app: App) {
    app.use(Icon.name as any, Icon)
    app.use(NavBar.name as any, NavBar)
    app.use(RadioButton.name as any, RadioButton)
    app.use(PaySheet.name as any, PaySheet)
  }
}
