import { LoginRes } from './login'

export interface OrderInfo {
  paidNumber: number
  receivedNumber: number
  shippedNumber: number
  finishedNumber: number
}

export interface User extends LoginRes {
  couponNumber: number
  score: number
  likeNumber: number
  collectionNumber: number
  account: string
  avatar: string
  id: string
  mobile: string
  consultationInfo: any[]
  orderInfo: OrderInfo
}

// 短信验证码类型
export type CodeType =
  | 'login'
  | 'register'
  | 'changeMobile'
  | 'forgetPassword'
  | 'bindMobile'
