import { MsgType } from '@/enums'
import { Consult } from './consult'
import { Patient } from './patient'

// 药品
export type Medical = {
  id: string
  name: string
  amount: string
  avatar: string
  specs: string
  usageDosag: string
  quantity: string
  prescriptionFlag: 0 | 1
}

// 处方
export type Prescription = {
  id: string
  orderId: string
  createTime: string
  name: string
  recordId: string
  gender: 0 | 1
  genderValue: ''
  age: number
  diagnosis: string
  status: PrescriptionStatus
  medicines: Medical[]
}

// 评价
export type EvaluateDoc = {
  id?: string
  score?: number
  content?: string
  createTime?: string
  creator?: string
}

// 单个消息
export interface Message {
  id?: string
  from?: string
  to?: string
  msgType: MsgType
  createTime: string
  msg: {
    id?: string
    content: string
    picture?: {
      id: string
      url: string
    }
    // 问诊信息
    consultRecord?: Consult & {
      patientInfo: Patient
    }
    // 处方
    prescription?: Prescription
    // 评价
    evaluateDoc?: EvaluateDoc
  }
}

// 消息列表
export interface TimeMessages {
  createTime: string
  items: Message[]
  orderId: string
  sid: string
}
