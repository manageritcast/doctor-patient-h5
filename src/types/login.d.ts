export interface LoginRes {
  token: string
  refreshToken: string
  avatar: string
  mobile: string
  account: string
  id: string
  bindingFlag: boolean
}
