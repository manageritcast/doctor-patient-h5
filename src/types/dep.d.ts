export interface Dep {
  id: string
  name: string
  avatar?: string
  child: Dep[]
}
