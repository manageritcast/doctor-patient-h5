import { Doctor } from './doctor'
import { Patient } from './patient'
import { IllnessTime } from '@/enums'
export interface Consult {
  /**
   * 开药门诊-过敏史0无1有2不清楚
   */
  allergicHistory?: number
  /**
   * 找医生/极速问诊-是否就诊过0未就诊1就诊过
   */
  consultFlag: 0 | 1
  /**
   * 使用优惠券，必传可使用优惠券id
   */
  couponId?: string
  /**
   * 选择的科室id-极速问诊必填(选择的科室)
   */
  depId?: string
  /**
   * 咨询的医生-类型为找医生问诊，字段是必须的
   */
  docId?: string
  /**
   * 开药门诊-生育状态及计划0无1备孕中2已怀孕3哺乳期
   */
  fertilityStatus?: number
  /**
   * 病情--症状描述
   */
  illnessDesc: string
  /**
   * 找医生/极速问诊-患病时间1一周内2一月内3半年内4半年以上
   */
  illnessTime: IllnessTime
  /**
   * 极速问诊类型：0普通1三甲
   */
  illnessType?: string
  /**
   * 开药门诊-肝功能0正常1异常2不清楚
   */
  liverFunction?: number
  /**
   * 患者id,用于关联患者信息
   */
  patientId: string
  /**
   * 补充病例信息-图片集合
   */
  pictures?: Picture[]
  /**
   * 开药门诊-肾功能0正常1异常2不清楚
   */
  renalFunction?: number
  /**
   * 就诊类型1找医生 2极速问诊 3开药问诊默认是1
   */
  type: string
}

export interface Picture {
  id: string
  url: string
}

export interface Image {
  id: string
  url: string
}

export interface PayInfo {
  actualPayment: number
  couponDeduction: nmber
  couponId: string
  payment: number
  pointDeduction: number
}

// 问诊订单单项信息
export type ConsultOrderItem = Consult & {
  id: string
  createTime: string
  docInfo?: Doctor
  patientInfo: Patient
  orderNo: string
  statusValue: string
  typeValue: string
  status: OrderType
  countdown: number
  prescriptionId?: string
  evaluateId: number
  payment: number
  couponDeduction: number
  pointDeduction: number
  actualPayment: number
}

export type ConsultOrderList = {
  pageTotal: number
  total: number
  rows: ConsultOrderItem[]
}
