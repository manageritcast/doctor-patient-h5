export interface Doctor {
  likeFlag: number
  id: string
  name: string
  avatar: string
  major: string
  score: string
  consultationNum: string
  serviceFee: string
  positionalTitles: string
  hospitalName: string
  gradeName: string
  depName: string
}

export interface Doctors {
  rows: Doctor[]
  total: number
  pageTotal: number
}
