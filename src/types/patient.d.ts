export interface Patient {
  name: string
  idCard: string
  defaultFlag: number
  gender: number
  genderValue?: string
  age?: number
  id: string
}
