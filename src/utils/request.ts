import axios, { AxiosError, Method } from 'axios'
import useStore from '@/store'
import router from '@/router'
import { ApiRes } from '@/types/data'

const instance = axios.create({
  // baseURL: 'https://consult-api.itheima.net/',
  baseURL: import.meta.env.VITE_BASEURL
})

// 添加请求拦截器
instance.interceptors.request.use(
  function (config) {
    // 在发送请求之前做些什么
    const { login } = useStore()
    const token = login.info.token
    if (token) {
      config.headers!.Authorization = `Bearer ${token}`
    }

    return config
  },
  function (error) {
    // 对请求错误做些什么
    return Promise.reject(error)
  }
)

// 添加响应拦截器
instance.interceptors.response.use(
  function (response) {
    // 这里千万不要加上.data，到时候需要拿到axios返回值类型
    return response
  },
  function (error: AxiosError<{ code: number }>) {
    // token失效了
    if (error.response?.data.code === 401) {
      const { login, user } = useStore()
      // 清除token和用户信息
      login.logout()
      user.logout()

      // 除开登录相关的页面，其它都跳转回登录页
      const fullPath = router.currentRoute.value.fullPath
      if (!fullPath.includes('login')) {
        router.push(
          `/login?redirect=${encodeURI(router.currentRoute.value.fullPath)}`
        )
      }
    }
    // 对响应错误做点
    return Promise.reject(error)
  }
)

export const request = <T>(
  url: string,
  method: Method = 'GET',
  data?: object
) => {
  return instance.request<ApiRes<T>>({
    url,
    method,
    [method.toLocaleLowerCase() === 'get' ? 'params' : 'data']: data
  })
}

export default instance
